<?php namespace Decoupled\Core\Extension\Action;

use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Application\ApplicationExtension;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionQueue;

class ActionExtension extends ApplicationExtension{

    public function getName()
    {
        return 'action.extension';
    }

    public function extend()
    {
        $app = $this->getApp();

        if( !isset($app['$action.invoker']) )
        {
            $app['$action.invoker'] = function(){

                return new ActionInvoker();
            };
        }

        $app['$action.queue'] = function(){

            return new ActionQueue();
        };

        $app['$action.factory'] = function($i){

            $factory = new ActionFactory();

            $invoker = $i['$action.invoker'];

            return $factory->setInvoker( $invoker );
        };

        $app->addExtensionTypeHandler( [
            'Closure', 
            'Decoupled\Core\Action\Action'
        ], [ new ActionExtensionHandler(), 'handle' ] );
            
    }
}