<?php namespace Decoupled\Core\Extension\Action;

use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Action\Action;

class ActionExtensionHandler{

    public function handle( $extension, ApplicationContainer $app )
    {
        if( !($extension instanceof Action) )
        {
            $extension = $app['$action.factory']->make( $extension );
        }

        call_user_func( $extension->bind($app) ); 
    }
}