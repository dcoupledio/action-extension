<?php

require('../vendor/autoload.php');

use phpunit\framework\TestCase;
use Decoupled\Core\Application\Application;
use Decoupled\Core\Action\ActionFactory;
use Decoupled\Core\Action\ActionInvoker;
use Decoupled\Core\Action\ActionQueue;
use Decoupled\Core\Application\ApplicationContainer;
use Decoupled\Core\Extension\Action\ActionExtension;

class ExtTest extends TestCase{

    public function testApplicationCanUseClosure()
    {
        $app = new Application( new ApplicationContainer() );

        $app->uses( new ActionExtension() );

        $app->uses(function(){

            $this['exampleService'] = function(){
                return 1;
            };
        });

        $this->assertEquals( $app['exampleService'], 1 );

        return $app;
    }

    /**
    * @depends testApplicationCanUseClosure
    **/

    public function testApplicationCanUseActionObject( $app )
    {
        $action = $app['$action.factory'];

        $app->uses( $action->make([ function(){

            $this['param'] = function() { return 1; };
        }]));

        $this->assertEquals( $app['param'], 1 );
    }

    /**
    * @depends testApplicationCanUseClosure
    **/    

    public function testAppCanUseQueue( $app )
    {
        $this->assertInstanceOf( ActionQueue::class, $app['$action.queue'] );
    }


}